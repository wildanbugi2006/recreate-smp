# Recreate SMP

Untuk Mencoba nya di localhost menggunakan XAMPP ada hal yang harus dilakukan 

1. Buatlah satu folder dalam folder htdocs di xampp (contoh : C:\xampp\htdocs)
2. Pindahkan semua file dan folder yang anda pull ke dalam folder yang tadi dibuat
3. Buka file 'wp-config.php' dan sesuaikan dengan database anda
4. Buka browser dan arahkan ke URL situs WordPress Anda di localhost.
   Pastikan situs berfungsi dengan baik dan semua konten ditampilkan dengan benar.

Screenshot Tampilan :

Beranda

![alt_text](Screenshot_2024-02-15_152240.png?raw=true)
![alt_text](Screenshot_2024-02-15_152523.png?raw=true)
![alt_text](Screenshot_2024-02-15_152607.png?raw=true)
![alt_text](Screenshot_2024-02-15_152712.png?raw=true)

Pos/Artikel

![alt_text](Screenshot_2024-02-15_192710.png?raw=true)
![alt_text](Screenshot_2024-02-15_193035.png?raw=true)

Data

![alt_text](Screenshot_2024-02-15_192831.png?raw=true)
![alt_text](Screenshot_2024-02-15_192913.png?raw=true)

Kontak

![alt_text](Screenshot_2024-02-15_193722.png?raw=true)




