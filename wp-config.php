<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '[ [+*,#{~o1>}T]V{78[YTVdZu`MDpMD^/VCPbl)h^aI3&=nvC~C[sxO~Mb/p4v]' );
define( 'SECURE_AUTH_KEY',  'm,-v[#q*GH`<+DM^.zN8[V<.JmQvaij)qf&DJ|qq_{.Dfl9cP`y/HD+R[=+FPBz#' );
define( 'LOGGED_IN_KEY',    'A:ghV~(>{ )jMEM^4gR%.HzXP;fS]M<>}N~5`Wb`5F~m=zsUP?u*6O7fV#5nIyo0' );
define( 'NONCE_KEY',        'Uw^}Ig/f@UA&F:r!tWO#kp^r8W7!H,aJ]@4tDFeNZXM!/<a6DdtQMq~W#D5NSaPT' );
define( 'AUTH_SALT',        'FfYY>PT`S @[ptJU0+yDiH*Tu9:Us2cSkZi=6;LK|5jK{{Wb92wz^{[OYBA$[SlY' );
define( 'SECURE_AUTH_SALT', '|yf3/&CJ+R|ck)itIu*HRk4jp?@DtX+:EDE7fJ<07Tvi,u2(jh1e6_`CT>+2@Z|=' );
define( 'LOGGED_IN_SALT',   'YHuNCqQQHrwylYzcUvDd~qL?,bW3wV0N]z}{^jef#+(GWR!VG1nNXyan^>$j)tYz' );
define( 'NONCE_SALT',       'K@`!t;Ig*AxRVV./B.nlhVe$p0H9U>q/uI11|/%m}fgV`HQ2p$BczAIIp32oghhO' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
